## Themes for Micro text editor

To change your colorscheme, press Ctrl-E in micro and type:
```
set colorscheme mixed
```